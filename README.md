Version:
  - node: v16.14.2
  - npm: 8.5.0
  - yarn: 1.22.18
  - react: 18.2.0

Config:
  - eslintrc
  - prettierrc
  - babel, webpack
  - typescript
  - editorconfig

=> Libraries to used for app:
  - React hook form
  - Redux saga
  - Yup validation
  - Lodashm monent
  - Loading skeleton, bootstrap, styled-component

Run local: 
  Step1 create file .evn, paste in : REACT_APP_API_URL=https://5f55a98f39221c00167fb11a.mockapi.io/
  Step2: yarn install
  Step3: yarn start

