export interface TableHeaderProps {
  id: string;
  label: string;
  align: 'left' | 'right' | 'center';
  icon?: React.ReactNode;
  width: number;
  hasSort?: boolean;
}
