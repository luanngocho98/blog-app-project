export interface BlogItem {
  id?: string;
  createdAt?: string;
  title: string;
  image: string;
  content: string;
}
