import { SnackbarState } from 'app/components/Snackbar/slice/types';
import { BlogState } from 'app/pages/Blog/slice/types';

export interface RootState {
  // [INSERT NEW REDUCER KEY ABOVE] < Needed for generating containers seamlessly
  snackbar?: SnackbarState;
  blogSlice?: BlogState;
}
