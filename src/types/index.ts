import { RootState } from './RootState';

export type { RootState };
export * from './Table';
export * from './Filter';

export interface Pageable<T> {
  data: T[];
  total: number;
}
