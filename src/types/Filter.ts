export interface FilterParams {
  page?: number;
  limit?: number;
  sortBy?: string;
  order?: string;
  search?: string;
}
