import { FilterParams } from 'types';
import { BlogItem } from 'types/Blog';
import { serialize } from 'utils/helpers';

import { createServiceNoToken } from './axios';

const instance = createServiceNoToken(process.env.REACT_APP_API_URL);

const fetchListBlog = async (params?: FilterParams): Promise<BlogItem[]> => {
  const res = await instance.get(`/blogs?${serialize(params)}`);
  return res.data;
};

const createBlog = async (payload: BlogItem) => {
  const _payload = new FormData();
  _payload.append('title', payload.title);
  _payload.append('content', payload.content);
  // _payload.append('image', payload.image);
  // => cheat image url because api not work when update file image
  _payload.append(
    'image',
    'https://imgv3.fotor.com/images/blog-cover-image/part-blurry-image.jpg',
  );
  const res = await instance.post(`/blogs`, _payload, {
    headers: { 'Content-Type': 'multipart/form-data' },
  });
  return res.data;
};

const deleteBlog = async (params: { blogId?: string }) => {
  const res = await instance.delete(`/blogs/${params.blogId}`);
  return res.data;
};

const getBlogDetai = async (params: { blogId?: string }): Promise<BlogItem> => {
  const res = await instance.get(`/blogs/${params.blogId}`);
  return res.data;
};

const editBlog = async (payload: BlogItem) => {
  const _payload = new FormData();
  _payload.append('title', payload.title);
  _payload.append('content', payload.content);
  // _payload.append('image', payload.image);
  // => cheat image url because api not work when update file image
  _payload.append(
    'image',
    'https://imgv3.fotor.com/images/blog-cover-image/part-blurry-image.jpg',
  );
  const res = await instance.put(`/blogs/${payload.id}`, _payload, {
    headers: { 'Content-Type': 'multipart/form-data' },
  });
  return res.data;
};

export default {
  fetchListBlog,
  createBlog,
  deleteBlog,
  getBlogDetai,
  editBlog,
};
