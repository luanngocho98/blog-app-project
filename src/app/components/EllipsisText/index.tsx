import React from 'react';
import styled from 'styled-components';

interface Props {
  text: string | number | null;
  line?: number;
}

const EllipsisTxt = styled('div')<{ line?: number }>`
  display: -webkit-box;
  -webkit-line-clamp: ${props => props.line};
  line-clamp: ${props => props.line};
  -webkit-box-orient: vertical;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: break-spaces;
  font-size: 1rem;
`;

function EllipsisText(props: Props) {
  const { text, line = 1 } = props;
  return <EllipsisTxt line={line}>{text}</EllipsisTxt>;
}

export default React.memo(EllipsisText);
