import CloseIcon from 'asset/icons/Close';
import React, { ChangeEvent, useState } from 'react';
import {
  Control,
  Controller,
  UseFormSetError,
  UseFormSetValue,
} from 'react-hook-form';
import styled from 'styled-components';

interface Props {
  name: string;
  control: Control<any>;
  isRequired?: boolean;
  label: string;
  isMultiple?: boolean;
  setError?: UseFormSetError<any>;
  setValue?: UseFormSetValue<any>;
}

const Image = styled('img')`
  object-fit: contain;
  width: inherit;
  height: inherit;
`;

const Contained = styled('div')`
  width: 100%;
  height: 500px;
  position: relative;
`;

const RemoveIcon = styled('span')`
  position: absolute;
  top: 10px;
  right: 10px;
`;

export default function UploadImage(props: Props) {
  const { name, control, isRequired, label, setError, setValue } = props;
  const [file, setFile] = useState<File | null>(null);

  const handleFileSelected = (event: ChangeEvent<HTMLInputElement>) => {
    if (event?.target?.files?.length) {
      const file = event?.target?.files?.[0];
      setFile(file);
      setError?.(name, { message: '' });
      setValue?.(name, file);
    }
    event.target.value = '';
  };

  const handleRemoveImage = () => {
    setFile(null);
  };

  return (
    <>
      <Controller
        name={name}
        control={control}
        rules={{
          validate: value => {
            if (isRequired && !file) {
              return `${label} is required`;
            }
            return true;
          },
        }}
        render={({ field, fieldState }) => {
          return (
            <>
              <small className="">
                {label} {isRequired && <span className="text-danger">*</span>}
              </small>
              <input
                onChange={handleFileSelected}
                className="form-control"
                type="file"
              />
              <small className="mt-1 text-danger">
                {fieldState?.error?.message || ''}
              </small>
            </>
          );
        }}
      />
      {file && (
        <Contained className="mt-4">
          <RemoveIcon onClick={handleRemoveImage}>
            <CloseIcon />
          </RemoveIcon>
          <Image alt="file uploaded" src={URL.createObjectURL(file)} />
        </Contained>
      )}
    </>
  );
}
