import { selectBlog } from 'app/pages/Blog/slice/selectors';
import NextIcon from 'asset/icons/Next';
import PreviousIcon from 'asset/icons/Previous';
import SortIcon from 'asset/icons/Sort';
import React, { Fragment } from 'react';
import Skeleton from 'react-loading-skeleton';
import { useDispatch, useSelector } from 'react-redux';
import { useSearchParams } from 'react-router-dom';
import { TableHeaderProps } from 'types/Table';
import styled from 'styled-components';

import { useSnackbarSlice } from '../Snackbar/slice';

const Contained = styled('div')`
  height: 720px;
  overflow: scroll;
`;

const Thead = styled('thead')`
  background: brown;
  color: white;
`;

const Th = styled('th')<{ width?: number }>`
  font-size: 0.938rem;
  width: ${props => props.width};
`;

interface Props {
  header: TableHeaderProps[];
  isLoading: boolean;
  items: any[];
  onPageChange: (page: number) => void;
  onRequestSort?: (property: string, orderType: string) => void;
  pageNumber?: number;
  renderItem: (item: any, index: number) => any[];
  limitNumber?: number;
}

function Table(props: Props) {
  const {
    header,
    isLoading = false,
    renderItem,
    items,
    onRequestSort,
    pageNumber = 1,
    limitNumber = 10,
    onPageChange,
  } = props;
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [searchParams, setSearchParams] = useSearchParams();
  const handleRequestSort = (
    event: React.MouseEvent<HTMLElement>,
    property: string,
  ) => {
    const orderType = searchParams?.get('order') === 'desc' ? 'asc' : 'desc';
    onRequestSort?.(property, orderType);
  };
  const dispatch = useDispatch();
  const { actions: actionsSnackbar } = useSnackbarSlice();
  const { blogManager } = useSelector(selectBlog);

  const handlePreviousPage = () => {
    if (pageNumber === 1) {
      dispatch(
        actionsSnackbar.updateSnackbar({
          open: true,
          message: 'You are on the first page',
          type: 'error',
        }),
      );
      return;
    }
    onPageChange?.(pageNumber - 1);
  };

  const handleNextPage = () => {
    if (blogManager && blogManager?.length < limitNumber) {
      dispatch(
        actionsSnackbar.updateSnackbar({
          open: true,
          message: 'You are on the end page',
          type: 'error',
        }),
      );
      return;
    }
    onPageChange?.(pageNumber + 1);
  };

  return (
    <div className="mt-2">
      {isLoading ? (
        <Skeleton height="calc(100vh - 260px)" width="100%" />
      ) : (
        <Fragment>
          <Contained>
            <table className="table table-hover">
              <Thead className="thead-dark">
                <tr>
                  {header?.map(col => (
                    <Th scope="col" key={col.id}>
                      {col.label} {col.icon}{' '}
                      {col.hasSort && (
                        <span
                          onClick={(event: React.MouseEvent<HTMLElement>) =>
                            handleRequestSort(event, col.id)
                          }
                        >
                          <SortIcon />
                        </span>
                      )}
                    </Th>
                  ))}
                </tr>
              </Thead>
              <tbody>
                {items?.map((row, indexRow) => (
                  <tr key={row.id}>
                    {renderItem(row, indexRow)?.map((col, indexCol) => (
                      <td key={row.id + indexCol}>{col}</td>
                    ))}
                  </tr>
                ))}
              </tbody>
            </table>
          </Contained>
          <div className="mt-4 text-end">
            <small className="ms-4">Page: {pageNumber}</small>
            <span className="ms-4" onClick={handlePreviousPage}>
              <PreviousIcon />
            </span>
            <span className="ms-2" onClick={handleNextPage}>
              <NextIcon />
            </span>
          </div>
        </Fragment>
      )}
    </div>
  );
}

export default React.memo(Table);
