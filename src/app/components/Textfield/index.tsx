import React from 'react';
import { Control, Controller } from 'react-hook-form';

interface Props {
  label: string;
  type?: 'text' | 'textarea';
  isRequired?: boolean;
  name: string;
  control: Control<any>;
  disabled?: boolean;
  rows?: number;
  placeholder: string;
  maxLength?: number;
}

function Textfield(props: Props) {
  const {
    label,
    type = 'text',
    isRequired = false,
    name,
    control,
    rows = 2,
    placeholder,
    maxLength = 200,
  } = props;

  return (
    <Controller
      name={name}
      control={control}
      rules={{
        validate: value => {
          if (isRequired && !value) {
            return `${label} is required`;
          }
          if (maxLength && value?.length > maxLength) {
            return `Max length is ${maxLength}`;
          }
          return true;
        },
      }}
      render={({ field, fieldState }) => {
        return (
          <>
            <small className="">
              {label} {isRequired && <span className="text-danger">*</span>}
            </small>
            {type === 'textarea' ? (
              <textarea
                className="w-100 form-control"
                placeholder={placeholder}
                {...field}
                rows={rows}
              />
            ) : (
              <input
                type={type}
                className="w-100 form-control"
                placeholder={placeholder}
                {...field}
              />
            )}
            <div className="d-flex justify-content-between mt-1">
              <small className="text-danger">
                {fieldState?.error?.message || ''}
              </small>
              <small className="d-block text-end">{`${
                field?.value?.length || 0
              }/${maxLength}`}</small>
            </div>
          </>
        );
      }}
    />
  );
}

export default React.memo(Textfield);
