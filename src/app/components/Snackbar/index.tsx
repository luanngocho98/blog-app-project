import React, { useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Alert } from 'reactstrap';
import useOnClickOutside from 'app/hooks/useOnClickOutside';

import { selectSnackbar } from './slice/selectors';
import { useSnackbarSlice } from './slice';

const Snackbar = () => {
  const { open, message, type } = useSelector(selectSnackbar);
  const { actions } = useSnackbarSlice();
  const dispatch = useDispatch();
  const handleClose = () => {
    dispatch(actions.closeSnackbar());
  };
  const ref = useRef<HTMLDivElement>(null);
  useOnClickOutside(ref, () => {
    handleClose();
  });

  return (
    <div ref={ref}>
      <Alert
        className={`snackbar ${open ? 'd-flex' : 'd-none'} ${
          type === 'error' ? 'bg-danger' : 'bg-success'
        }`}
        color="primary"
      >
        {message}
      </Alert>
    </div>
  );
};

export default React.memo(Snackbar);
