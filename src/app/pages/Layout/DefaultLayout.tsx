import { Outlet } from 'react-router-dom';

export default function DefaultLayout() {
  return (
    <div className="container">
      <div className="p-3 mt-4">
        <Outlet />
      </div>
    </div>
  );
}
