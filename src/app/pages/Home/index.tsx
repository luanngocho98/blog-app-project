import path from 'app/routes/path';
import React, { Fragment } from 'react';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';

import { useBlogSlice } from '../Blog/slice';

export default function Home() {
  const navigate = useNavigate();
  const { actions } = useBlogSlice();
  const dispatch = useDispatch();

  const redirectPage = (url: string) => () => {
    dispatch(actions.startLoading());
    navigate(url);
  };

  return (
    <Fragment>
      <div className="d-flex flex-wrap justify-content-between">
        <h2>Welcome to My Blog App</h2>
        <div>
          <button
            type="button"
            className="btn btn-danger"
            onClick={redirectPage(path.blog)}
          >
            Table list
          </button>
          <button
            type="submit"
            className="btn btn-primary ms-2"
            onClick={redirectPage(path.blogLoadInfinity)}
          >
            Load infinity
          </button>
        </div>
      </div>
    </Fragment>
  );
}
