/* eslint-disable jsx-a11y/img-redundant-alt */
import React from 'react';
import { BlogItem } from 'types/Blog';
import styled from 'styled-components';
import { useNavigate } from 'react-router-dom';

interface Props {
  blog: BlogItem;
}

const Item = styled('div')`
  height: 300px;
  width: 400px;
  background: brown;
  cursor: pointer;
`;

const Title = styled('h5')`
  cursor: pointer;
`;

function BlogRow(props: Props) {
  const { blog } = props;
  const navigate = useNavigate();
  const replaceImage = (error: any) => {
    error.target.src =
      'https://www.salonlfc.com/wp-content/uploads/2018/01/image-not-found-1-scaled-1150x647.png';
  };
  const redirectDetailPage = () => {
    navigate(`/blog-detail/${blog.id}`);
  };
  return (
    <>
      <li className="media">
        <Item>
          <img
            src={blog.image}
            className="mr-3"
            alt="Image Blog"
            onError={replaceImage}
            height="300px"
            width="400px"
            onClick={redirectDetailPage}
          />
        </Item>
        <div className="media-body mt-2">
          <Title onClick={redirectDetailPage} className="mt-0 mb-1">
            {blog.title}
          </Title>
          {blog.content}
        </div>
      </li>
      <hr className="mb-4" />
    </>
  );
}

export default React.memo(BlogRow);
