/* eslint-disable jsx-a11y/img-redundant-alt */
import React from 'react';
import Skeleton from 'react-loading-skeleton';
import styled from 'styled-components';

interface Props {
  count: number;
}

const SkeletonCustom = styled(Skeleton)`
  height: 300px;
  width: 400px;
  @media only screen and (max-width: 600px) {
    width: 100%;
  }
`;

export default function CustomSkeleton(props: Props) {
  const { count } = props;
  return (
    <ul className="list-unstyled">
      {Array(count)
        .fill(0)
        ?.map((item, index) => (
          <li className="media" key={item + index}>
            <SkeletonCustom />
            <div className="media-body mt-2">
              <Skeleton width="100%" height="30px" />
              <Skeleton width="100%" height="35px" />
            </div>
          </li>
        ))}
    </ul>
  );
}
