import { useFilter } from 'app/hooks/useFilter';
import Filterbar from 'app/pages/Blog/components/Filterbar';
import { useBlogSlice } from 'app/pages/Blog/slice';
import { selectBlog } from 'app/pages/Blog/slice/selectors';
import React, { Fragment, useMemo, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { FilterParams } from 'types';
import InfiniteScroll from 'react-infinite-scroll-component';

import BlogRow from '../BlogRow';

import CustomSkeleton from '../CustomSkeleton';

export default function BlogList() {
  const dispatch = useDispatch();
  const [page, setPage] = useState<number>(1);
  const { actions } = useBlogSlice();
  const { blogManager, isLoading } = useSelector(selectBlog);
  const initialFilter = useMemo(() => {
    return {
      page: 1,
      limit: 5,
      order: 'desc',
    };
  }, []);

  const { filter, onFilterToQueryString } = useFilter({
    onFetchData: (params: FilterParams) => {
      fetchDataForPage(params);
    },
    defaultFilter: initialFilter,
  });

  const fetchDataForPage = (params: FilterParams) => {
    dispatch(actions.fetchListBlog(params));
  };

  const handleSearchBlog = (keyword: string) => {
    onFilterToQueryString({
      ...filter,
      search: keyword,
      page: 1,
    });
  };

  const getMorePosts = () => {
    const params = {
      ...filter,
      page: page + 1,
    };
    setPage(page + 1);
    dispatch(actions.loadMoreBlog(params));
  };

  const hasMore = useMemo(() => {
    // because BE k returns total set which defaults to true
    return true;
  }, []);

  return (
    <Fragment>
      <h2>Load infinity</h2>
      <Filterbar
        placeholder="Please enter the data you want to search ~!~"
        handlSearch={handleSearchBlog}
      />
      <div className="mt-4">
        {isLoading ? (
          <CustomSkeleton count={4} />
        ) : (
          <InfiniteScroll
            dataLength={blogManager?.length || 0}
            next={getMorePosts}
            hasMore={hasMore}
            loader={<CustomSkeleton count={1} />}
          >
            <ul className="list-unstyled">
              {blogManager?.map(blog => (
                <div key={blog.id}>
                  <BlogRow blog={blog} />
                </div>
              ))}
            </ul>
          </InfiniteScroll>
        )}
      </div>
    </Fragment>
  );
}
