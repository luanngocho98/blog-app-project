import { lazyLoad } from 'utils/loadable';

export const BlogLoadInfinity = lazyLoad(
  () => import('./index'),
  module => module.BlogLoadInfinity,
);
