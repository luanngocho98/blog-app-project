import { useSnackbarSlice } from 'app/components/Snackbar/slice';
import Textfield from 'app/components/Textfield';
import UploadImage from 'app/components/UploadImage';
import CloseIcon from 'asset/icons/Close';
import React, { Fragment, useEffect, useState } from 'react';
import { SubmitErrorHandler, useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { BlogItem } from 'types/Blog';
import styled from 'styled-components';

import { useBlogSlice } from '../slice';
import { selectBlog } from '../slice/selectors';
interface Props {
  isEdit?: boolean;
}

const Image = styled('img')`
  object-fit: contain;
  width: inherit;
  height: inherit;
`;

const Contained = styled('div')`
  width: 100%;
  height: 500px;
  position: relative;
  background: grey;
`;

const RemoveIcon = styled('span')`
  position: absolute;
  top: 10px;
  right: 10px;
`;

export default function CreateBlog(props: Props) {
  const { isEdit } = props;
  const blogForm = useForm<BlogItem>({
    mode: 'onChange',
  });
  const { actions } = useBlogSlice();
  const { actions: actionsSnackbar } = useSnackbarSlice();
  const { blogDetail } = useSelector(selectBlog);
  const [isDisable, setIsDisable] = useState(false);
  const dispatch = useDispatch();
  const { control, handleSubmit, setError, setValue, reset } = blogForm;
  const navigate = useNavigate();
  const goBack = () => {
    dispatch(actions.startLoading());
    navigate(-1);
    dispatch(actions.clearDataBlogDetail());
  };
  const [isShowDetailImage, setIsShowDetailImage] = useState<boolean>(() => {
    if (!!blogDetail && blogDetail?.image) {
      return true;
    }
    return false;
  });

  useEffect(() => {
    if (!isEdit) return;
    const _blogDetail = { ...blogDetail };
    delete _blogDetail?.image;
    reset(blogDetail);
  }, [blogDetail, isEdit, reset]);

  useEffect(() => {
    return () => {
      dispatch(actions.clearDataBlogDetail());
    };
  }, [actions, dispatch]);

  const onSubmit = (data: BlogItem) => {
    setIsDisable(true);
    if (isEdit) {
      dispatch(
        actions.editBlog(data, (response?: any) => {
          if (response?.success) {
            navigate(-1);
            dispatch(
              actionsSnackbar.updateSnackbar({
                open: true,
                type: 'success',
                message: 'Edit blog is successfully',
              }),
            );
          } else {
          }
          setIsDisable(false);
        }),
      );
    } else {
      dispatch(
        actions.createBlog(data, (response?: any) => {
          if (response?.success) {
            navigate(-1);
            dispatch(
              actionsSnackbar.updateSnackbar({
                open: true,
                type: 'success',
                message: 'Create blog is successfully',
              }),
            );
          } else {
          }
          setIsDisable(false);
        }),
      );
    }
  };

  const replaceImage = (error: any) => {
    error.target.src =
      'https://www.salonlfc.com/wp-content/uploads/2018/01/image-not-found-1-scaled-1150x647.png';
  };

  const handleRemoveImage = () => {
    setIsShowDetailImage(false);
  };

  const onError: SubmitErrorHandler<BlogItem> = errors => {
    if (errors) {
      dispatch(
        actionsSnackbar.updateSnackbar({
          open: true,
          message: 'Please recheck data information',
          type: 'error',
        }),
      );
    }
  };

  return (
    <Fragment>
      <form onSubmit={handleSubmit(onSubmit, onError)}>
        <div className="d-flex justify-content-between">
          <h2>{isEdit ? 'Edit Blog' : 'Create Blog'}</h2>
          <div>
            <button type="button" className="btn btn-danger" onClick={goBack}>
              Back
            </button>
            <button
              type="submit"
              className="btn btn-primary ms-1"
              disabled={isDisable}
            >
              {isEdit ? 'Save' : 'Create'}
            </button>
          </div>
        </div>
        <div className="mt-4">
          <div className="mb-3">
            <Textfield
              name="title"
              placeholder="Please enter blog title"
              type="text"
              control={control}
              label="Title"
              isRequired
              maxLength={100}
            />
          </div>
          <div className="mb-3">
            <Textfield
              name="content"
              placeholder="Please enter blog content"
              type="textarea"
              control={control}
              label="Content"
              isRequired
              rows={3}
              maxLength={300}
            />
          </div>
          <div className="mb-3">
            {isShowDetailImage ? (
              <Contained className="mt-4">
                <RemoveIcon onClick={handleRemoveImage}>
                  <CloseIcon />
                </RemoveIcon>
                <Image
                  alt="file uploaded"
                  src={blogDetail?.image}
                  onError={replaceImage}
                />
              </Contained>
            ) : (
              <UploadImage
                name="image"
                control={control}
                label="Image"
                isRequired
                setError={setError}
                setValue={setValue}
              />
            )}
          </div>
        </div>
      </form>
    </Fragment>
  );
}
