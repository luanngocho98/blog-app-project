import React, { Fragment, useEffect } from 'react';
import Skeleton from 'react-loading-skeleton';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate, useParams } from 'react-router-dom';
import styled from 'styled-components';

import { useBlogSlice } from '../slice';
import { selectBlog } from '../slice/selectors';

const Image = styled('img')`
  object-fit: contain;
  width: inherit;
  height: inherit;
`;

const BoxImage = styled('div')`
  background: grey;
  width: 100%;
  height: 500px;
  position: relative;
`;

export default function DetailBlog() {
  const { blogId } = useParams();
  const dispatch = useDispatch();
  const { actions } = useBlogSlice();
  const { isLoading, blogDetail } = useSelector(selectBlog);
  const navigate = useNavigate();

  useEffect(() => {
    if (!blogId) return;
    dispatch(actions.getBlogDetai({ blogId }));
  }, [actions, blogId, dispatch]);

  const goBack = () => {
    dispatch(actions.startLoading());
    navigate(-1);
  };

  useEffect(() => {
    return () => {
      dispatch(actions.clearDataBlogDetail());
    };
  }, [actions, dispatch]);

  const redirectEditPage = () => {
    if (!blogId) return;
    navigate(`/blog-edit/${blogId}`);
  };

  const replaceImage = (error: any) => {
    error.target.src =
      'https://www.salonlfc.com/wp-content/uploads/2018/01/image-not-found-1-scaled-1150x647.png';
  };

  return (
    <Fragment>
      {isLoading ? (
        <Skeleton height="calc(100vh - 165px)" width="100%" />
      ) : (
        <>
          <div className="d-flex justify-content-between">
            <h2>Blog Detail</h2>
            <div>
              <button type="button" className="btn btn-danger" onClick={goBack}>
                Back
              </button>
              <button
                type="submit"
                className="btn btn-primary ms-1"
                onClick={redirectEditPage}
              >
                Edit
              </button>
            </div>
          </div>
          <div className="mt-4">
            <h3 className="text-center text-danger">{blogDetail?.title}</h3>
            <BoxImage className="mt-4">
              <Image
                alt="file uploaded"
                src={blogDetail?.image}
                onError={replaceImage}
              />
            </BoxImage>
            <h5 className="text-center text-secondar mt-4">
              {blogDetail?.content}
            </h5>
          </div>
        </>
      )}
    </Fragment>
  );
}
