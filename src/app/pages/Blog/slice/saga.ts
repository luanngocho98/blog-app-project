import { PayloadAction } from '@reduxjs/toolkit';
import { call, put, takeLatest } from 'redux-saga/effects';
import blog from 'services/api/blog';
import { FilterParams } from 'types';
import { BlogItem } from 'types/Blog';

import { contractActions as actions } from '.';

function* fetchListBlog(action: PayloadAction<FilterParams>) {
  try {
    const result: BlogItem[] = yield call(blog.fetchListBlog, action.payload);
    yield put(actions.fetchListBlogSuccess(result));
  } catch (errors) {}
}

function* createBlog(
  action: PayloadAction<BlogItem, string, (error?: any) => void>,
) {
  try {
    yield call(blog.createBlog, action.payload);
    yield put(actions.createBlogSuccess());
    action.meta({
      success: true,
    });
  } catch (error: any) {
    action.meta({
      success: false,
      response: error.response,
    });
  }
}

function* deleteBlog(
  action: PayloadAction<{ blogId?: string }, string, (error?: any) => void>,
) {
  try {
    yield call(blog.deleteBlog, action.payload);
    yield put(actions.deleteBlogSuccess());
    action.meta({
      success: true,
    });
  } catch (error: any) {
    action.meta({
      success: false,
      response: error.response,
    });
  }
}

function* getBlogDetai(action: PayloadAction<{ blogId?: string }>) {
  try {
    const result: BlogItem = yield call(blog.getBlogDetai, action.payload);
    yield put(actions.getBlogDetaiSuccess(result));
  } catch (errors) {}
}

function* editBlog(
  action: PayloadAction<BlogItem, string, (error?: any) => void>,
) {
  try {
    yield call(blog.editBlog, action.payload);
    yield put(actions.editBlogSuccess());
    action.meta({
      success: true,
    });
  } catch (error: any) {
    action.meta({
      success: false,
      response: error.response,
    });
  }
}

function* loadMoreBlog(action: PayloadAction<FilterParams>) {
  try {
    const result: BlogItem[] = yield call(blog.fetchListBlog, action.payload);
    yield put(actions.loadMoreBlogSuccess(result));
  } catch (errors) {}
}

export function* blogSaga() {
  yield takeLatest(actions.fetchListBlog.type, fetchListBlog);
  yield takeLatest(actions.createBlog.type, createBlog);
  yield takeLatest(actions.deleteBlog.type, deleteBlog);
  yield takeLatest(actions.getBlogDetai.type, getBlogDetai);
  yield takeLatest(actions.editBlog.type, editBlog);
  yield takeLatest(actions.loadMoreBlog.type, loadMoreBlog);
}
