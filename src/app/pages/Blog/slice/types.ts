import { BlogItem } from 'types/Blog';

export interface BlogState {
  blogManager?: BlogItem[];
  isLoading?: boolean;
  blogDetail?: BlogItem;
}
