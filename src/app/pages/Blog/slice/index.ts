import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { useInjectReducer, useInjectSaga } from 'utils/redux-injectors';
import { BlogItem } from 'types/Blog';

import { BlogState } from './types';
import { blogSaga } from './saga';

export const initialState: BlogState = {
  isLoading: true,
};

const slice = createSlice({
  name: 'blogSlice',
  initialState,
  reducers: {
    startLoading(state) {
      state.isLoading = true;
    },
    endLoading(state) {
      state.isLoading = false;
    },
    fetchListBlog(state, action) {
      state.isLoading = true;
    },
    fetchListBlogSuccess: (state, action: PayloadAction<BlogItem[]>) => {
      state.blogManager = action.payload;
      state.isLoading = false;
    },
    createBlog: {
      reducer(state) {
        return state;
      },
      prepare(params: BlogItem, meta: (error?: any) => void) {
        return { payload: params, meta };
      },
    },
    createBlogSuccess: state => {
      state.isLoading = true;
    },
    deleteBlog: {
      reducer(state) {
        return state;
      },
      prepare(params: { blogId?: string }, meta: (error?: any) => void) {
        return { payload: params, meta };
      },
    },
    deleteBlogSuccess: state => {
      state.isLoading = true;
    },
    getBlogDetai(state, action) {
      state.isLoading = true;
    },
    getBlogDetaiSuccess: (state, action) => {
      state.isLoading = false;
      state.blogDetail = action.payload;
    },
    clearDataBlogDetail(state) {
      state.blogDetail = undefined;
    },
    editBlog: {
      reducer(state) {
        return state;
      },
      prepare(params: BlogItem, meta: (error?: any) => void) {
        return { payload: params, meta };
      },
    },
    editBlogSuccess: state => {
      state.isLoading = true;
    },
    loadMoreBlog(state, action) {},
    loadMoreBlogSuccess: (state, action: PayloadAction<BlogItem[]>) => {
      const oldBlogs = state.blogManager || [];
      state.blogManager = oldBlogs.concat(action.payload);
    },
  },
});

export const useBlogSlice = () => {
  useInjectReducer({ key: slice.name, reducer: slice.reducer });
  useInjectSaga({ key: slice.name, saga: blogSaga });
  return { actions: slice.actions };
};

export const { actions: contractActions } = slice;
