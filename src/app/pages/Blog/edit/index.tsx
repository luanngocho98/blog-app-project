import React, { Fragment, useEffect } from 'react';
import Skeleton from 'react-loading-skeleton';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';

import CreateBlog from '../create';
import { useBlogSlice } from '../slice';
import { selectBlog } from '../slice/selectors';

export default function EditBlog() {
  const { blogId } = useParams();
  const dispatch = useDispatch();
  const { actions } = useBlogSlice();
  const { isLoading } = useSelector(selectBlog);

  useEffect(() => {
    if (!blogId) return;
    dispatch(actions.getBlogDetai({ blogId }));
  }, [actions, blogId, dispatch]);

  return (
    <Fragment>
      {isLoading ? (
        <Skeleton height="calc(100vh - 165px)" width="100%" />
      ) : (
        <CreateBlog isEdit />
      )}
    </Fragment>
  );
}
