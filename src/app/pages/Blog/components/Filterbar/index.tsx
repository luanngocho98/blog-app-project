import path from 'app/routes/path';
import BackIcon from 'asset/icons/Back';
import React, { ChangeEvent } from 'react';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';

import { useBlogSlice } from '../../slice';

interface Props {
  placeholder: string;
  handlSearch: (keyword: string) => void;
}

export default function Filterbar(props: Props) {
  const { placeholder, handlSearch } = props;
  const navigate = useNavigate();
  const { actions } = useBlogSlice();
  const dispatch = useDispatch();

  const redirectCreatePage = () => {
    dispatch(actions.startLoading());
    navigate(path.createBlog);
  };

  const backHomePage = () => {
    navigate(path.home);
  };

  const handleChangeInput = (event: ChangeEvent<HTMLInputElement>) => {
    handlSearch?.(event.target.value);
  };

  return (
    <div className="d-flex flex-row justify-content-between">
      <div className="w-50">
        <input
          type="text"
          className="form-control"
          id="basic-url"
          aria-describedby="basic-addon3"
          placeholder={placeholder}
          onChange={handleChangeInput}
        />
      </div>
      <div className="ms-2">
        <span onClick={backHomePage}>
          <BackIcon />
        </span>
        <button
          type="button"
          className="btn btn-info ms-2"
          onClick={redirectCreatePage}
        >
          Add New
        </button>
      </div>
    </div>
  );
}
