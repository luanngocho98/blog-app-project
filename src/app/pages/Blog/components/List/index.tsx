/* eslint-disable jsx-a11y/img-redundant-alt */
import EllipsisText from 'app/components/EllipsisText';
import { useSnackbarSlice } from 'app/components/Snackbar/slice';
import Table from 'app/components/Table';
import { useFilter } from 'app/hooks/useFilter';
import DeleteIcon from 'asset/icons/Delete';
import ViewIcon from 'asset/icons/View';
import moment from 'moment';
import React, { Fragment, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { BlogItem } from 'types/Blog';
import { FilterParams } from 'types/Filter';
import { TableHeaderProps } from 'types/Table';
import styled from 'styled-components';

import { useBlogSlice } from '../../slice';
import { selectBlog } from '../../slice/selectors';

import Filterbar from '../Filterbar';

const Image = styled('img')`
  width: 120px;
  height: 70px;
  background: brown;
`;

export default function BlogList() {
  const dispatch = useDispatch();
  const { actions } = useBlogSlice();
  const { actions: actionsSnackbar } = useSnackbarSlice();
  const navigate = useNavigate();
  const { blogManager, isLoading } = useSelector(selectBlog);
  const header: TableHeaderProps[] = useMemo(() => {
    return [
      {
        id: 'stt',
        label: 'STT',
        align: 'left',
        width: 50,
      },
      {
        id: 'title',
        label: 'Title',
        align: 'left',
        width: 200,
        hasSort: true,
      },
      {
        id: 'image',
        label: 'Image',
        align: 'left',
        width: 150,
      },
      {
        id: 'content',
        label: 'Content',
        align: 'left',
        width: 300,
        hasSort: true,
      },
      {
        id: 'createdAt',
        label: 'Created At',
        align: 'left',
        width: 100,
      },
      {
        id: 'actions',
        label: 'Actions',
        align: 'left',
        width: 50,
      },
    ];
  }, []);
  const initialFilter = useMemo(() => {
    return {
      page: 1,
      limit: 10,
      order: 'desc',
    };
  }, []);

  const { filter, onFilterToQueryString } = useFilter({
    onFetchData: (params: FilterParams) => {
      fetchDataForPage(params);
    },
    defaultFilter: initialFilter,
  });

  const fetchDataForPage = (params: FilterParams) => {
    dispatch(actions.fetchListBlog(params));
  };

  const replaceImage = (error: any) => {
    error.target.src =
      'https://www.salonlfc.com/wp-content/uploads/2018/01/image-not-found-1-scaled-1150x647.png';
  };

  const handleDelete = (blogId?: string) => () => {
    dispatch(
      actions.deleteBlog({ blogId }, (response?: any) => {
        if (response?.success) {
          dispatch(actions.fetchListBlog(filter));
          dispatch(
            actionsSnackbar.updateSnackbar({
              open: true,
              type: 'success',
              message: 'Delete blog is successfully',
            }),
          );
        } else {
        }
      }),
    );
  };

  const handleSearchBlog = (keyword: string) => {
    onFilterToQueryString({
      ...filter,
      search: keyword,
      page: 1,
    });
  };

  const redirectToPageDetailBlog = (blogId?: string) => () => {
    dispatch(actions.startLoading());
    navigate(`/blog-detail/${blogId}`);
  };

  const renderItem = (item: BlogItem, index: number) => {
    const limit = filter?.limit || 0;
    const page = filter?.page || 0;
    return [
      <div>
        <EllipsisText text={index + 1 + limit * page - limit} line={1} />
      </div>,
      <div>
        <EllipsisText text={item.title} line={2} />
      </div>,
      <Image
        width="120px"
        height="70px"
        alt="Image row"
        src={item.image}
        onError={replaceImage}
      />,
      <div>
        <EllipsisText text={item.content} line={2} />
      </div>,
      <div>
        <EllipsisText
          text={moment(item.createdAt).format('DD/MM/YYYY')}
          line={1}
        />
      </div>,
      <div className="d-flex">
        <span onClick={redirectToPageDetailBlog(item.id)}>
          <ViewIcon />
        </span>
        <span className="ms-2" onClick={handleDelete(item.id)}>
          <DeleteIcon />
        </span>
      </div>,
    ];
  };

  const onPageChange = (page: number) => {
    dispatch(actions.startLoading());
    onFilterToQueryString({
      ...filter,
      page,
    });
  };

  const onRequestSort = (property: string, orderType: string) => {
    onFilterToQueryString({
      ...filter,
      sortBy: property,
      order: orderType,
    });
  };

  return (
    <Fragment>
      <h2>Table list</h2>
      <Filterbar
        placeholder="Please enter the data you want to search ~!~"
        handlSearch={handleSearchBlog}
      />
      <div className="mt-4">
        <Table
          header={header}
          isLoading={!!isLoading}
          items={blogManager || []}
          onPageChange={onPageChange}
          onRequestSort={onRequestSort}
          pageNumber={filter.page}
          limitNumber={filter.limit}
          renderItem={renderItem}
        />
      </div>
    </Fragment>
  );
}
