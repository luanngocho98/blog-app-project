import NotFound from 'app/pages/404';
import { BlogLoadInfinity } from 'app/pages/BlogLoadInfinity/Loadable';
import CreateBlog from 'app/pages/Blog/create';
import DetailBlog from 'app/pages/Blog/detail';
import EditBlog from 'app/pages/Blog/edit';
import { Blog } from 'app/pages/Blog/Loadable';
import Home from 'app/pages/Home';
import DefaultLayout from 'app/pages/Layout/DefaultLayout';
import ProtectedLayout from 'app/pages/Layout/ProtectedLayout';
import React from 'react';
import { useLocation, Navigate, useRoutes } from 'react-router';

import path from './path';

export default function Router() {
  const location = useLocation();
  const isAuthenticated = false;
  return useRoutes([
    {
      path: path.root,
      element: isAuthenticated ? (
        <ProtectedLayout />
      ) : (
        <Navigate to={path.home} state={{ from: location }} />
      ),
      children: [],
    },
    {
      path: path.root,
      element: <DefaultLayout />,
      children: [
        {
          path: path.home,
          element: <Home />,
        },
        {
          path: path.blogLoadInfinity,
          element: <BlogLoadInfinity />,
        },
        {
          path: path.blog,
          element: <Blog />,
        },
        {
          path: path.createBlog,
          element: <CreateBlog />,
        },
        {
          path: path.editBlog,
          element: <EditBlog />,
        },
        {
          path: path.detailBlog,
          element: <DetailBlog />,
        },
        { path: path.notFound, element: <NotFound /> },
      ],
    },
    { path: path.all, element: <Navigate to={path.notFound} replace /> },
  ]);
}
