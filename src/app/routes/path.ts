const path = {
  home: '/home',
  blogLoadInfinity: '/blog-infinity',
  blog: '/blog-list',
  createBlog: '/blog-create',
  editBlog: '/blog-edit/:blogId',
  detailBlog: '/blog-detail/:blogId',
  notFound: '/not-found',
  root: '/',
  all: '*',
};

export default path;
