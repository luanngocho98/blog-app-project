import React from 'react';
import { Helmet } from 'react-helmet-async';
import { SkeletonTheme } from 'react-loading-skeleton';
import { BrowserRouter } from 'react-router-dom';
import 'react-loading-skeleton/dist/skeleton.css';

import Snackbar from './components/Snackbar';
import Routes from './routes';
export default function App() {
  return (
    <SkeletonTheme baseColor="#E3E3E3" highlightColor="#e6ebe7">
      <BrowserRouter>
        <Helmet titleTemplate="Blog-App" defaultTitle="Blog-App">
          <meta name="description" content="A Blog application" />
        </Helmet>
        <Routes />
        <Snackbar />
      </BrowserRouter>
    </SkeletonTheme>
  );
}
